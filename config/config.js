var CONFIG = {
	"defaultConfig": {
		"resizableFlag": false,
		"smallMovePX": 1,
		"testPwd": "root",
		"language": "zh_cn"
	},
	"defaultStyle": {
		"selectedBoxShadow": "rgb(0, 0, 0) 0px 0px 20px 0px",
		"noSelectedBoxShadow": "",
		"dragOpacity": 0.8
	},
	"conn": {
		"stroke": "#2a2929",
		"strokeWidth": 3,
		"hoverConnStroke": "blue",
		"connectionType": "Flowchart",
		"connectionGap": 5,
		"connectionCornerRadius": 8,
		"connectionAlwaysRespectStubs": true,
		"connectionStub": 30,
		"isDetachable": false
	},
	"arrow": {
		"arrowWidth": 12,
		"arrowLength": 12,
		"arrowLocation": 1
	},
	"endPonit": {
		"fill": "#456",
		"stroke": "#2a2929",
		"strokeWidth": 1,
		"radius": 3,
		"hoverEndPointStroke": "pink"
	},
	"anchors": {
		"defaultAnchors": ["Bottom", "Right", "Top", "Left"],
		"sourceAnchors": [],
		"targetAnchors": [],
		"sameAnchorsFlag": true
	},
	"alignParam": {
		"levelDistance": 100,
		"verticalDistance": 100,
		"alignDuration": 500
	},
	"keyboardParam": {
		"multipleSelectedKey": 17,
		"deleteKey": 46,
		"upKey": 38,
		"downKey": 40,
		"leftKey": 37,
		"rightKey": 39,
		"undoKey": 90,
		"redoKey": 89,
		"selectedAllKey": 65,
		"saveKey": 83,
		"save2PhotoKey": 80,
		"clearKey": 68,
		"showGridKey": 81,
		"mouseToolKey": 81,
		"connectionToolKey": 82,
		"settingKey": 70
	}
}